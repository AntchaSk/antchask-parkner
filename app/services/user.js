/**
 * This service is used to do http requests to the user endpoint.
 */

(function() {
  'use strict';

  angular
    .module('user.service', [])
    .service('UserService', UserService)

  UserService.$inject = ['$http', 'AUTH0_SIGNUP_ENDPOINT', 'PARKNER_API_BASE_PATH', 'API_BASE_PATH'];

  function UserService($http, AUTH0_SIGNUP_ENDPOINT, PARKNER_API_BASE_PATH) {
    let service = this;

    // Creates a new user in Auth0 using this endpoint:
    // https://YOUR_AUTH0_DOMAIN/dbconnections/signup
    service.postUserAuth0 = function(form) {
      return $http
        .post(AUTH0_SIGNUP_ENDPOINT, form)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    //                                                                      
    // security endpoint                                                    
    //                                                                      

    // POST /security/register
    service.postUser = function(user) {
      const URL = PARKNER_API_BASE_PATH + '/security/register';
      return $http
        .post(URL, user)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // if hasBeenRegistered
    service.hasBeenRegistered = function() {
      if (localStorage.getItem('is_registered')) {
        return true;
      }
    }

    //                                                                      
    // /user                                                                
    //                                                                      

    // GET /user
    service.getUsers = function() {
      const URL = PARKNER_API_BASE_PATH + '/user';
      return $http
        .get(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // DELETE /user/{id}
    service.deleteUser = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/user' + '/' + userId;
      return $http
        .delete(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // GET /user/{id}
    service.getUserById = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/user' + '/' + userId;
      return $http
        .get(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    //                                                                      
    // /user/{id}/paymentInfo                                               
    //                                                                      

    // PUT /user/{id}/paymentInfo
    service.putUserPaymentInfo = function(userId, paymentInfo) {
      const URL = PARKNER_API_BASE_PATH + '/user' + '/' + userId;
      return $http
        .put(URL + '/paymentInfo?bankName=' + paymentInfo.bankName + '&bankNumber=' + paymentInfo.bankNumber)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // Get the user id
    service.getUserId = function(email) {
      const URL = PARKNER_API_BASE_PATH + '/user';
      return $http
        .get(URL)
        .then(function(response) {
          let userId = "";
          let arrays = response.data;
          for (let array of arrays) {
            if (email == array.email) {
              userId = array.id;
              break;
            }
          }
          return userId;
        }, function(error) {
          return error;
        });
    };

    // Check if user has payment information
    service.getUserPaymentInfo = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/user';
      return $http
        .get(URL)
        .then(function(response) {
          let hasPaymentInfo = false;
          let arrays = response.data;
          for (let array of arrays) {
            if (userId == array.id) {
              if (array.bankName && array.bankNumber) {
                hasPaymentInfo = true;
                break;
              }
            }
          }
          return hasPaymentInfo;
        }, function(error) {
          return error;
        });
    };

    // Chech if user has a credit card registered
    service.getUserCard = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/user';
      return $http
        .get(URL)
        .then(function(response) {
          let hasCard = false;
          let arrays = response.data;
          for (let array of arrays) {
            if (userId == array.id) {
              if (array.cards && array.cards.length !== 0) {
                hasCard = true;
              }
            }
          }
          return hasCard;
        }, function(error) {
          return error;
        });
    };

    //                                                                      
    //  /user/{id}/cars                                                     
    //                                                                      

    service.getUserCars = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/user' + '/' + userId + '/cars';
      return $http
        .get(URL)
        .then(function(response) {
          let arrays = response.data;
          let vehicles = [];
          for (let array of arrays) {
            if (array != null) {
              vehicles.push(array);
            }
          }
          return vehicles;
        }, function(error) {
          return error;
        });
    };

    service.postUserCar = function(userId, car) {
      const URL = PARKNER_API_BASE_PATH + '/user' + '/' + userId + '/cars';
      return $http
        .post(URL, car)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    }

  }

})();
