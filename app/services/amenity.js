/**
 * This service is used to provide an array of amenities.
 * TODO: Fetch them from the API.
 */

(function() {
  'use strict';

  angular
    .module('amenity.service', [])
    .service('AmenityService', AmenityService);

  AmenityService.$inject = ['$http', 'PARKNER_API_BASE_PATH']

  function AmenityService($http, PARKNER_API_BASE_PATH) {
    let service = this;

    service.getAmenities = function() {
      const amenities = [{
          "id": "598a6e65579d673294657d31",
          "description": "Estacionamiento Techado",
          "name": "Techado",
          "icon": "icn-garage-with-roof"
        },
        {
          "id": "598a6e76579d673294657d32",
          "description": "Camaras De Seguridad",
          "name": "Camaras",
          "icon": "icn-security-camera"
        },
        {
          "id": "598a6e7c579d673294657d33",
          "description": "Llave de Estacionamiento Entregada",
          "name": "Llave",
          "icon": "icn-key"
        },
        {
          "id": "598a6e83579d673294657d34",
          "description": "Personal de Seguridad",
          "name": "Seguridad",
          "icon": "icn-policeman"
        },
        {
          "id": "598a6e8a579d673294657d35",
          "description": "Lavado de Auto",
          "name": "Lavado de Auto",
          "icon": "icn-car-wash"
        },
        {
          "id": "598a6e8f579d673294657d36",
          "description": "Abierto 24 horas",
          "name": "24 Horas",
          "icon": "icn-24-hours"
        }
      ];

      return amenities;
      /*return $http
          .get(PARKNER_API_BASE_PATH + '/amenity')
          .then(function(response) {
              return response;
          }, function(error) {
              return error;
          });*/
    };

  }

})();
