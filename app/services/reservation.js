/**
 * This service is used to do http requests to the reservation endpoint.
 */

(function() {
  'use strict';

  angular
    .module('reservation.service', [])
    .service('ReservationService', ReservationService);

  ReservationService.$inject = ['$http', 'PARKNER_API_BASE_PATH', 'API_BASE_PATH'];

  function ReservationService($http, PARKNER_API_BASE_PATH, API_BASE_PATH) {
    let service = this;

    //                                                                      
    // /reservation endpoint                                                
    //                                                                      

    // GET /reservation
    service.getReservations = function() {
      const URL = PARKNER_API_BASE_PATH + '/reservation';
      return $http
        .get(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // POST /reservation
    service.postReservation = function(reservation) {
      const URL = PARKNER_API_BASE_PATH + '/reservation';
      return $http
        .post(URL, reservation)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // DELETE /reservation/{id}
    service.deleteReservation = function(reservationId) {
      const URL = PARKNER_API_BASE_PATH + '/reservation' + '/' + reservationId;
      return $http
        .delete(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    }

    // GET /api/reservation/{id}
    service.getReservationById = function(reservationId) {
      const URL = PARKNER_API_BASE_PATH + '/reservation' + '/' + reservationId;
      return $http
        .get(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // POST /api/reservation/{id}/accept
    service.postReservationAccept = function(reservationId) {
      const URL = PARKNER_API_BASE_PATH + '/reservation' + '/' + reservationId + '/accept';
      return $http
        .post(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // POST /api/reservation/{id}/reject
    service.postReservationReject = function(reservationId) {
      const URL = PARKNER_API_BASE_PATH + '/reservation' + '/' + reservationId + '/reject';
      return $http
        .post(URL)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    // GET /reservation/{id}
    service.getMyReservations = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/reservation/' + userId;
      return $http
        .get(URL)
        .then(function(response) {
          let reservations = [];
          let objects = response.data;
          if (objects.length !== 0) {
            for (let obj of objects) {
              let reservation = {};

              let images = obj.images;
              let amenities = obj.amenities;
              let id = obj.id;
              let customer = obj.customer;
              let provider = obj.provider;
              let details = obj.details;
              let totalCost = obj.totalCost;
              let type = obj.type;
              let money = obj.money;
              let car = obj.car;
              let lotId = obj.lotId;
              let lotDetails = obj.lotDetails;

              reservation = {
                description: details.address.description,
                startDate: details.startDate,
                endDate: details.endDate,
                active: details.active,
                totalCost: totalCost,
              };

              reservations.push(reservation);
            }
          }
          return reservations;
        }, function(error) {
          return error;
        });
    };

  }

})();
