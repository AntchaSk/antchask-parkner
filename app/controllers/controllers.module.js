/**
 * Here we register all the controllers used in the application.
 */

(function() {
  'use strict';

  angular
    .module('app.controllers', [
      'current.controller',
      'collect-history.controller',
      'find.controller',
      'help.controller',
      'lot-details.controller',
      'lot-listing.controller',
      'map.controller',
      'parent.controller',
      'payment-history.controller',
      'profile.controller',
      'register-lot.controller',
      'register-user.controller',
      'register-vehicle.controller',
      'reservation-listing.controller',
      'successful-lot-registration.controller',
      'successful-user-registration.controller',
      'successful-vehicle-registration.controller',
      'vehicle-listing.controller'
    ]);

})();