/**
 * It shows a success message when a new user has been registered.
 */

(function() {
  'use strict';

  angular
    .module('successful-user-registration.controller', [])
    .config(function($stateProvider) {
      $stateProvider
        .state('successful-user-registration', {
          url: '/successful-user-registration',
          templateUrl: 'app/views/successful-user-registration.html',
          controller: 'SuccessfulUserRegistrationController as vm',
          data: {
            pageTitle: 'Registro Exitoso',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("successful-user-registration", "successful-user-registration.css");
            }]
          }
        });
    })
    .controller('SuccessfulUserRegistrationController', SuccessfulUserRegistrationController);

  SuccessfulUserRegistrationController.$inject = [];

  function SuccessfulUserRegistrationController() {}

})();
