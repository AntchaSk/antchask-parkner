/**
 * Display the user profile.
 * Data is fixed because there's no endpoint to save this information.
 */

(function() {
  'use strict';

  angular
    .module('profile.controller', ['ui.router', 'chart.js'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.profile', {
          url: '/profile',
          templateUrl: 'app/views/profile.html',
          controller: 'ProfileController as vm',
          data: {
            pageTitle: 'Perfil',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("profile", "profile.css");
            }]
          }
        });
    })
    // chart
    .config(['ChartJsProvider', function(ChartJsProvider) {
      // Configure all charts
      ChartJsProvider.setOptions({
        chartColors: ["#102b95", "#ffffff"],
        responsive: true
      });
      // Configure all line charts
      ChartJsProvider.setOptions('line', {
        showLines: true
      });
    }])
    .controller('ProfileController', ProfileController);

  function ProfileController() {
    let vm = this;
    //userInfo
    vm.profilePicture = 'app/img/background.jpg';
    vm.ocupacion = "Jubilada";
    vm.informacionVerificada = 'Correo Electronico, celular, documento de identidad';
    vm.userPicture = "http://via.placeholder.com/200x200";
    vm.username = "Samira Dawali";
    vm.fecha = "Enero 2017";
    vm.numeroComentarios = "5";
    vm.aboutMe = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever";
    //friendInfo
    vm.friendName = "Ricardo";
    vm.fechaComment = "Noviembre 2017";
    vm.friendComment = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of";
    vm.misCalificaciones = "50";
    vm.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
    vm.data = [35, 45];
    vm.dataUsuario = [45, 50];
  }

})();
